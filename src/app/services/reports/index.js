import baseService from '../../core/service/request'
import { cached } from '../../core/service/storage'

const appid = 'c5e180ceadbaa5602c694d2b33180b14'

const group = baseService('group')

export const getReportsFrom = (cities) => group({ queryParams: { id: cities.join(','), units: 'metric', appid } })
export const getCachedReportsFrom = cached(getReportsFrom)