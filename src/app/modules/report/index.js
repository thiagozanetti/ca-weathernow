import m from 'mithril'

import { format } from '../../core/helpers/datetime'

import './report.css'

const temperature = (weather) => {
  if (weather <= 5) {
    return 'colder'
  } else if (weather > 5 && weather <= 25) {
    return 'warm'
  } else {
    return 'warmer'
  }
}

export default {
  view(vnode) {
    const weather = vnode.attrs.weather || {}

    return m('section.report', [
      m('header.report-header', `${weather.name}, ${weather.sys.country}`),
      m('section.report-temperature', { class: temperature(weather.main.temp) }, `${Math.round(weather.main.temp)}°`),
        m('label.report-footer.humidity__label', 'HUMIDITY'),
        m('section.report-footer.humidity__p',  [
          `${weather.main.humidity}`,
          m('span', '%')
        ]),
        m('label.report-footer.pressure__label', 'PRESSURE'),
        m('section.report-footer.pressure__p', [
          `${Math.round(weather.main.pressure)}`,
          m('span', 'hPa'),
        ]),
      m('footer.report-footer.lastupdate', `Updated at: ${format(weather.lastUpdate, 'hh:mm:ss A')}`),
      ])
  }
}