global.window = Object.assign(require('mithril/test-utils/domMock.js')(), require('mithril/test-utils/pushStateMock')())

import mq from 'mithril-query'

import Report from '.'

describe('Testing modules', () => {
  let lastUpdate, weather = null
  describe('Report module', () => {

    beforeEach(() => {
      lastUpdate = new Date().valueOf()

      weather = {
        lastUpdate,
        name: 'Florianópolis',
        sys: {
          country: 'BR',
        },
        main: {
          temp: 25,
          humidity: 94,
          pressure: 867,
        },
      }
    })

    it('renders a proper structured report component', () => {
      const out = mq(Report, { weather })

      out.should.have([
        'section.report',
        'header.report-header',
        'section.report-temperature',
        'label.report-footer.humidity__label',
        'section.report-footer.humidity__p',
        'label.report-footer.pressure__label',
        'section.report-footer.pressure__p',
        'footer.report-footer.lastupdate',
      ])
    })

    it('renders valid values for the report component', () => {
      const out = mq(Report, { weather })

      out.should.contain('Florianópolis, BR')
      out.should.contain('25')
      out.should.contain('94')
      out.should.contain('867')
    })

    it('correctly changes the temperature class for the given value', () => {
      mq(Report, { weather }).should.have('section.report-temperature.warm')

      weather.main.temp = 4
      mq(Report, { weather }).should.have('section.report-temperature.colder')

      weather.main.temp = 26
      mq(Report, { weather }).should.have('section.report-temperature.warmer')
    })
  })
})
