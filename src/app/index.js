import m from 'mithril'

import Report from './modules/report'
import { getCachedReportsFrom } from './services/reports'
import { cities, expiration } from './core/constants'
import { diff, due } from './core/helpers/datetime'

import './app.css'

const citiesIds = Object.keys(cities).map((e) => cities[e].id)

let reports = { list: [] }

let timeout = -1

const retrieveData = () => getCachedReportsFrom(citiesIds).then((result) => {
  reports = result

  timeout = setTimeout(() => retrieveData(), diff(due(result.lastUpdate, expiration)))

  m.redraw()
})

export default {
  oncreate() {
    retrieveData()
  },
  ondestroy() {
    clearTimeout(timeout)
  },
  view(vnode) {
    return m('section.content', reports.list.map(weather => m(Report, { weather })))
  }
}