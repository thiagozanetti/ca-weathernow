import m from 'mithril'

const WHEATHERNOW_SERVICE_URL = process.env.WHEATHERNOW_SERVICE_URL || 'https://api.openweathermap.org/data/2.5/'

const baseService = (resource = '', baseUrl = WHEATHERNOW_SERVICE_URL, method = 'GET') => (params = {}) => {
  // spreads params and sets the default values if not found
  let { data = {}, urlParams = [], queryParams = [], headers = {}, trailingSlash = false } = params

  // reduces url params (for url based endpoints)
  let reducedUrlParams = urlParams.reduce((acc, item) => acc = acc.concat(`${item}/`), '/')

  // if the resource doesn't allow trailing slashes before query params, we must remove then.
  reducedUrlParams = !trailingSlash ? reducedUrlParams.slice(0, -1) : reducedUrlParams

  // reduces query params (for query based endpoints)
  const reducedQueryParams = Object.keys(queryParams).reduce((acc, key) => acc = acc.concat(`${key}=${queryParams[key]}&`), '').slice(0, -1)

  // builds the final url
  const url = `${baseUrl}${resource}${reducedUrlParams}${!!reducedQueryParams ? `?${reducedQueryParams}` : ''}`

  const request = () => m.request({ method, url, data, headers })

  return {
    request,
    url,
  }
}

export default baseService