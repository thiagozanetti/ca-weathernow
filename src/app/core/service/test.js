global.window = Object.assign(require('mithril/test-utils/domMock.js')(), require('mithril/test-utils/pushStateMock')())

import chai from 'chai'

import { createStorage } from '../helpers/test-utils/local-storage'

import baseService from './request'
import { cached } from './storage'

describe('Testing services', () => {
  describe('Request service', () => {
    it('returns the default service url if no params given', () => {
      const expected = 'https://api.openweathermap.org/data/2.5/'
      const baseTestService = baseService()
      const { url } = baseTestService()

      chai.expect(url).to.equal(expected)
    })

    it('returns the default request object if no params given', () => {
      const baseTestService = baseService()
      const { request } = baseTestService()
      const actual = request()

      chai.expect(actual).to.be.an('object').and.have.keys('_instance', 'then')
    })

    it('returns the given service url and resource', () => {
      const expected = 'http://test.ing/api/v1/resource'
      const testingResourceService = baseService('resource', 'http://test.ing/api/v1/')
      const { url } = testingResourceService()

      chai.expect(url).to.equal(expected)
    })

    it('returns the given service url with url params', () => {
      const expected = 'http://test.ing/api/v1/resource/path/to/some/endpoint'
      const testingResourceService = baseService('resource', 'http://test.ing/api/v1/')
      const { url } = testingResourceService({ urlParams: ['path', 'to', 'some', 'endpoint'] })

      chai.expect(url).to.equal(expected)
    })

    it('returns the given service url with query params', () => {
      const expected = 'http://test.ing/api/v1/resource?param1=value1&param2=value2'
      const testingResourceService = baseService('resource', 'http://test.ing/api/v1/')
      const { url } = testingResourceService({ queryParams: { param1: 'value1', param2: 'value2' } })

      chai.expect(url).to.equal(expected)
    })

    it('returns the given service url with a single query param', () => {
      const expected = 'http://test.ing/api/v1/resource?param1=value1'
      const testingResourceService = baseService('resource', 'http://test.ing/api/v1/')
      const { url } = testingResourceService({ queryParams: { param1: 'value1' } })

      chai.expect(url).to.equal(expected)
    })

    it('returns the full given service url', () => {
      const expected = 'http://test.ing/api/v1/resource/path/to/some/endpoint?param1=value1&param2=value2'
      const testingResourceService = baseService('resource', 'http://test.ing/api/v1/')
      const { url } = testingResourceService({
        urlParams: ['path', 'to', 'some', 'endpoint'],
        queryParams: { param1: 'value1', param2: 'value2' }
      })

      chai.expect(url).to.equal(expected)
    })
  })

  describe('Storage service', () => {
    it ('caches the result value for reusing', () => {
      const storage = createStorage()

      const serviceMock = () => ({
        url: 'http://test.ing/api/v1/resource/path/to/some/endpoint?param1=value1&param2=value2',
        request: () => ({
          then: (func) => func({ list: [{ id: 1 }] })
        })
      })

      const encoded = btoa(serviceMock().url)

      const cachedService = cached(serviceMock, storage)

      cachedService()

      chai.expect(storage.getItem(encoded)).to.not.null
    })

    it ('throws an error if the given param is not a function', () => {

      chai.expect(cached({})).to.throw(TypeError)
    })
  })
})