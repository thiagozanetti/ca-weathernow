import moment from 'moment'

import { expiration } from '../../constants'
import { expired } from '../../helpers/datetime'

const parsed = (key, storage) => (storage.getItem(key) && JSON.parse(storage.getItem(key))) || null

const isValid = (result) => result && !expired(result.lastUpdate, expiration)

const cacheReportValues = (key, value, storage) => {
  const lastUpdate = moment().valueOf()

  const list = value.list.map((element) => {
    element['lastUpdate'] = lastUpdate

    return element
  })

  storage.setItem(key, JSON.stringify({ lastUpdate, list }))
}

// injecting storage mechanism so we can easily mock it for test purposes
export const cached = (func, storage = window.localStorage) => params => {
  if (typeof(func) !== 'function') {
    throw new TypeError('func must be a function')
  }

  // prepares the service to be called if necessary
  const service = func(params)

  // encodes the url to work as a key for this cached value
  const key = btoa(service.url)

  // returns the parsed value (if any) from cache or null
  let result = parsed(key, storage)

  // checks if the result is valid and has not expired yet and returns imediately
  if (isValid(result)) {
    return Promise.resolve(result)
  }

  // if no value found or expired, make the request to the service
  result = service.request()

  // save the value retrieved so the application can use it from cache
  result.then((obj) => cacheReportValues(key, obj, storage))

  return result
}
