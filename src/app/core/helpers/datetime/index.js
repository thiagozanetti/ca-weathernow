import moment from 'moment'

export const diff = (to, from = moment(), unit = 'milliseconds') => from.diff(to, unit)

export const due = (time, duration, unit = 'minutes') => moment(time).add(duration, unit)

export const format = (time, mask) => moment(time).format(mask)

export const fromNow = (time) => diff(moment(time))

export const expired = (time, minutes) => fromNow(due(time, minutes)) >= 0
