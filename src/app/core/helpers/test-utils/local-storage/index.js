export const createStorage = () => {
  const storage = {}

  return {
    storage,
    setItem: (key, value) => storage[key] = value,
    getItem: (key) => storage[key] || null,
  }
}