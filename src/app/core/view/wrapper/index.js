import m from 'mithril';

import Header from '../header';
import Container from '../container';

import './wrapper.css';

export default {
  view(vnode) {

  return m('main',
    [
      Header,
      m(Container, vnode.children),
    ]);
  }
};