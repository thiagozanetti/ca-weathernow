import m from 'mithril'

import logo from './logo.svg'

import './header.css'

export default m('header.header', m('img', { src: logo }));
