
import m from 'mithril';

import './container.css'

export default {
  view(vnode) {
    return m('.container', vnode.children);
  }
};