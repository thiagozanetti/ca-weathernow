const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const webpack = require('webpack');

const paths = require('.');

const WP_SERVER_PORT = parseInt(process.env.WP_SERVER_PORT) || 3000;

module.exports = {
  devtool: 'inline-source-map',
  entry: path.resolve(paths.srcPath(), 'index.js'),
  output: {
    filename: 'app.bundle.js',
    path: paths.distPath(),
  },
  module: {
    rules: [{
        test: /\.js$/,
        exclude: /node_modules/,
        loader: "babel-loader"
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader']
      },
      {
        test: /\.svg$/,
        loader: 'svg-url-loader',
        options: {
          stripdeclarations: true,
          iesafe: true,
          noquotes: true,
        }
      }
    ]
  },
  plugins: [
    new CleanWebpackPlugin(),
    new HtmlWebpackPlugin({
      template: path.resolve(paths.publicPath(), 'index.html')
    }),
    new webpack.NamedModulesPlugin(),
    new webpack.HotModuleReplacementPlugin(),
  ],
  devServer: {
    contentBase: paths.distPath(),
    hot: true,
    port: WP_SERVER_PORT,
  }
};