# ca-weathernow

"No one else more dedicated, I'm the weatherman" [Weatherman,  from Dead Sara](https://www.youtube.com/watch?v=y5vr_Vhoumc)

This is a challenge app using [mithril.js](https://mithril.js.org) and the [Open Weather Map](http://www.openweathermap.com) API to retrieve weather conditions in three different places.

![ca-wheaternow](https://user-images.githubusercontent.com/333482/36451117-b851fe42-166e-11e8-9fb8-7080cec11d44.png)


## How to use

```sh
$ npm install
$ npm start
```

A new browser window will open at http://localhost:3000/

That is it!